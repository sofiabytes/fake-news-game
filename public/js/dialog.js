var conversation=document.getElementById("dialogText");
var tutorialP1;

var person


function makeButton(person, id){
    if (person.replies[id].hasOwnProperty('required')){
        console.log("Getting journal status...")
        var stat=retrieveInfoStatus(person.replies[id].required[0]);
        console.log(stat)
        if (stat==person.replies[id].required[1]){
            console.log("Yay!")
        }
        else {
            return ""
        }
    }
    console.log(person.replies[id])
    if (person.replies[id].hasOwnProperty('goto')){
    return "<br/><button class='dialogButtons' id='" + id + "' onclick=\"loadDialog(person, person.replies[id].goto);\">" + person.replies[id].text + "</button>";}
    if (person.replies[id].hasOwnProperty('level')){
    return "<br/><button class='dialogButtons' id='" + id + "' onclick=\"loadLevel(person.replies[id].level, person.replies[id].stage);\">" + person.replies[id].text + "</button>";}
}



function loadDialog(person, id){
    console.log("Id:"+id)
    var text=person.dialogues[id].text   
    var textString=""
    console.log("Type of text:" + text)
    console.log(typeof(text))
    if (typeof(text)=='string'){
        textString="<p>"+text+"</p>"
    }
    else if (typeof(text)=='object'){
        textString="<p>"+text.join('</p><p>')+"</p>"
    }
    else {
        console.log(typeof(text))
    }
    var replies=person.dialogues[id].replies
    var button=""
    console.log(replies)
    for (let i in replies){
        button+=makeButton(person,replies[i]);
    }
    conversation.innerHTML=textString+button
}



function start_dialogue(file){
    console.log("New Dialog  "+file+" Double check")
    $.getJSON(file, function(data){
        person = data;
        console.log(person)
        loadDialog(person, person.start_id)
    });

}
/*
    $.getJSON('json/dialogue.json', function(data){
        tutorialP1 = data.tutorialP1;
    });
    
function loadDialog(text, replies){
    conversation.innerHTML = text+replies;

    document.getElementById("tutorialP1-responseCrazy").onclick = function(){loadDialog(answerCrazy, responses);};

    document.getElementById("tutorialP1-responseWhy").onclick = function(){loadDialog(answerWhy, responses);};

    var a = document.getElementById("tutorialP1-sesame");
    if (a){
        a.onclick = function(){saveToJournal(tutorialP1.misc00, "tutorialP1", "tutorialP1.misc00", "miscTable");
    };}

    var b = document.getElementById("tutorialP1-please");
    if (b){
        b.onclick = function(){saveToJournal(tutorialP1.misc01, "tutorialP1", "tutorialP1.misc01", "miscTable");
    };}

    var c = document.getElementById("tutorialP1-magic");
    if (c){
        c.onclick = function(){saveToJournal(tutorialP1.verbal00, "tutorialP1", "tutorialP1.verbal00", "verbalTable");
    };}

    var d = document.getElementById("tutorialP1-corrupt");
    if (d){
        d.onclick = function(){saveToJournal(tutorialP1.verbal01, "tutorialP1", "tutorialP1.verbal01", "verbalTable");
    };}

}

var startText = "<span id='dialogtext'>\n\
    <p>Hello, Lord Demon! I have summoned you to help me kill Dark Lord V!</p>\n\
    <p>To ensure your aid, you are trapped in here with me until you swear on your unholy name to help.</p></span>";

var responses = dialogButtons("tutorialP1-responseCrazy", "Let me out of here, crazy lady!") 
        + dialogButtons("tutorialP1-responseWhy", "You want me to what? Why?");

var answerCrazy = "<p>How rude. You can only get out of here using the correct lock and password combination. \n\
    Good luck with that. Why not try running around and "
    + clickText("tutorialP1-sesame", "shouting the phrase Open Sesame")
    + " and see if that works?</p>";
    
var answerWhy = "<p>You see, this world is a magical world, but " 
    + clickText("tutorialP1-magic", "magic is fading and everything is dying with it!")
    +"</p><p>"
    + clickText("tutorialP1-corrupt", "Magic was originally pure Light, but Sorcerors corrupted magic for their own purposes.") 
    + "</p><p>If you don't believe me, go ahead and investigate this world. \n\
    You can determine the truth by clicking on what you think is relevant info. \n\
    If it is truly relevant, it will automatically be added to your journal, to the right.</p>\n\
    <p>However, relevant does not mean it is true! \n\
    I'm sure your Demon Lordship is smart enough to find the truth, but to help you, \n\
    you can adjust the sliders next to each bit of saved info to remind yourself if you judge it trustworthy or false.</p>\n\
    <p>Now go! " 
    + clickText("tutorialP1-please", "The password to get out is 'Down With Sorcerors'!") 
    + " I've forgotten what colour the correct lock is, however... Maybe try standing on them all?</p>";

loadDialog(startText, responses);
*/
