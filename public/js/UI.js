//The two containers to show or hide
var map = document.getElementById("mapview");
var texts = document.getElementById("textview");
//1 when map is displayer, 0 when dialog/journal is displayed
var swap = 1;

//Show text interface and hide map
function showUI(){
	map.style.display="none";
	texts.style.display="block";
	swap=0;
}

//Show map and hide text interface;
function hideUI(){
	map.style.display="block";
	texts.style.display="none";
	swap=1;
}

function isUI(){
	if (swap){
		return false;
	}
	else {return true}
	
}

//Switch between the two states
function swapUI(){
	if (swap){
		showUI();
		console.log("Switching to text interface");
	} else {
		hideUI();
		console.log("Switching to map interface");
	}
	//swap = 1-swap;
	//console.log(swap);
}
