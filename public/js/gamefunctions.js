//////////////////////Variables/////////////////////////////////
//List of images
////////////////////////Functions//////////////////////////
function resizeCanvas(){
	height=window.innerHeight*0.8;
	width=window.innerWidth*0.9;
	c.width=width;
	c.height=height;
        //Centers canvas on player
	offsetX=width/2-player.x;
	offsetY=height/2-player.y;
	ctx.transform(1, 0, 0, 1, offsetX, offsetY);
}


//Load Level Function
function loadLevel(levelJSON, stageID) {
    currentlevel=levelJSON
    currentstage=stageID
    //Get images
    $.getJSON('json/images.json', function(data) {
        for (var i in data){
            var tmp=new Image();
            tmp.src=data[i];
            imgs[i]=tmp;
        }
    });
    
    //Get player data
    $.getJSON('json/player.json', function(data){
        player=data;
        //player.x=(width/2);
    });
    
    //Get level data
    $.getJSON(levelJSON, function(data){
        leveldata = data;
    });
    
    //Get and set the default text for the current level
    levelText = leveldata.levelText;
    document.getElementById("dialogText").innerHTML=levelText;
    
    //Get links to the correct JSON files for objects and obstacles by stage
    objsJSON=leveldata.objects[stageID];
    obstJSON=leveldata.obstacles[stageID];
    
    //Get objects data
    $.getJSON(objsJSON, function(data){
        objs=data;
    });
    
    //Get obstacles data
    $.getJSON(obstJSON, function(data){
        obsts=data;
    });
    
    //Resets transformation of canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    
    //Centers canvas on player
    offsetX=width/2-player.x;
    offsetY=height/2-player.y;
    ctx.transform(1, 0, 0, 1, offsetX, offsetY);
	hideUI();
}

//Draw Background
function paintBG(){
    //Clears entire visible canvas
    ctx.clearRect(player.x-width, player.y-height, width*2, height*2);
    //Redraws visible canvas background
    ctx.fillStyle="black";
    ctx.fillRect(player.x-width, player.y-height, width*2, height*2);
    //Draw floor plan
    floor = leveldata.floor;
    ctx.fillStyle=floor.color;
    ctx.fillRect(floor.x, floor.y, floor.width, floor.height);

}

//Draw Player
function drawPlayer(){
    ctx.drawImage(imgs[player.src], player.x, player.y, player.width, player.height);
    //For debugging - view player co-ords  
    //ctx.font = "30px Comic Sans MS";
    //ctx.fillStyle = "red";
    //ctx.fillText("X:"+player.x+" Y:"+player.y,player.x,player.y)    
}

//Draw level objects
function drawObjs(obj){
    var o;
    for (o in obj){
        if (obj[o].hasOwnProperty('img')){
            //console.log('Image object: '+obj[o]);
            ctx.drawImage(imgs[obj[o].img], obj[o].x, obj[o].y, obj[o].width, obj[o].height);
        }
        else if (obj[o].hasOwnProperty('color')){
            //console.log('Rectangle object'+obj[o].color+obj[o].x);
            ctx.fillStyle=obj[o].color;
            ctx.fillRect(obj[o].x, obj[o].y, obj[o].width, obj[o].height);
        }
        else {
            console.log(obj[o] + " has no draw property");
        }
        //For Debugging - displays x and y co-ords by object
        //ctx.font = "30px Comic Sans MS";
        //ctx.fillStyle = "red";
        //ctx.fillText("X:"+obj[o].x+" Y:"+obj[o].y,obj[o].x,obj[o].y)
    }
}

//Draw level obstacles
function drawWalls(obj){
    var w;
    for (w in obj){
        if (obj[w].hasOwnProperty('img')){
            //console.log('Image object: '+obj[o]);
            ctx.drawImage(imgs[obj[w].img], obj[w].x, obj[w].y, obj[w].width, obj[w].height);
        }
        else if (obj[w].hasOwnProperty('color')){
            //console.log('Rectangle object'+obj[o].color+obj[o].x);
            ctx.fillStyle=obj[w].color;
            ctx.fillRect(obj[w].x, obj[w].y, obj[w].width, obj[w].height);
        }
        else {
            console.log(obj[w] + " has no draw property");}
    }
}

//Calculate whether two objects are colliding and returns true if yes
function collide(objA, objB){
    if (objA.x < objB.x + objB.width &&
        objA.x + objA.width > objB.x &&
        objA.y < objB.y + objB.height &&
        objA.y + objA.height > objB.y) {
            return true;
    }
}

//Add dialog to dialog box when player collides with an object
function writeObjs(obj, player, current){
    var o;
    var defaultText=true;
    for (o in obj){
        if (obj[o].scriptsrc!==null){
            if (collide(obj[o], player)) {
                if (obj[o].hasOwnProperty('scriptsrc')){
                    defaultText=false;
                    //console.log('Player:' + x);
                    //console.log('Current object:' + obj[o].x);
                    if (current!==o){
                        start_dialogue(obj[o].scriptsrc)
//                        var dynamicScript=document.createElement("script");
  //                      console.log(current);
    //                    dynamicScript.setAttribute("id", "dynamic");
      //                  dynamicScript.src=obj[o].scriptsrc;
        //                document.head.appendChild(dynamicScript);
			if (obj[o].hasOwnProperty('img')){
                        document.getElementById("icon").src=imgs[obj[o].img].src;}
                        current=o;
                        break;
                    }
                }
            }
        }
    }
    if (defaultText && current !== null){
        current=null;
        console.log('No text');
        //document.head.removeChild(document.getElementById("dynamic"));
        document.getElementById("icon").src=imgs["player"].src;
        document.getElementById("dialogText").innerHTML=levelText;
    }
    return current;
}

//Causes player to repel when colliding with obstacles
function wallCollisions(obst, player, velX, velY){
    for (var o in obst){
        if (collide(player, obst[o])){

                if (player.x < obst[o].x + obst[o].width &&
					player.x > obst[o].x &&
                    player.x + player.width > obst[o].x + obst[o].width){
                        player.x = obst[o].x + obst[o].width;
                        ctx.transform(1, 0, 0, 1, -velX, 0);
                        //console.log("a, player.x:"+player.x);
                    }
                else if (player.x + player.width > obst[o].x &&
					player.x + player.width < obst[o].x + obst[o].width &&
                    player.x < obst[o].x){
                        player.x = obst[o].x - player.width;
                        ctx.transform(1, 0, 0, 1, -velX, 0);
                        //console.log("b, player.x:"+player.x);
                    }

                else if (player.y < obst[o].y + obst[o].height &&
                    player.y + player.height > obst[o].y + obst[o].height){
                        player.y = obst[o].y + obst[o].height;          
                        ctx.transform(1, 0, 0, 1, 0, -velY);
                        //console.log("c, player.x:"+player.x);
                    }
                else if (player.y + player.height > obst[o].y &&
                    player.y < obst[o].y){
                        player.y = obst[o].y - player.height;        
                        ctx.transform(1, 0, 0, 1, 0, -velY);
                        //console.log("d, player.x:"+player.x);
                }
            
        }
    }		
}

//Set key listeners
function setKeyboard(){
    document.body.addEventListener("keydown", function(e) {
        keys[e.keyCode] = true;
    });

    document.body.addEventListener("keyup", function(e) {
        keys[e.keyCode] = false;
    });
}

function keyDownActions(e){
	var keyCode = e.keyCode;
	if (isUI()){
	switch (keyCode){
		case 32:
			hideUI();
			break;
		default:
			console.log("Unknown input: "+ keyCode);
	}
	}
	else{
		switch (keyCode){
		case 32:
			showUI();
			player.velX=0;
			player.velY=0;
			break;
		case 65:
//		case 37:
			player.velX=4;
			break;
		case 68:
//		case 39:
			player.velX=-4;
			break;
		case 87:
//		case 38:
			player.velY=4;
			break;
		case 83:
//		case 40:
			player.velY=-4;
			break;
		default:
			console.log("Unknown input: "+ keyCode);
	}
		
	}
}

function keyUpActions(e){
	var keyCode = e.keyCode;
	switch (keyCode){
		case 65:
//		case 37:
		case 68:
//		case 39:
			player.velX=0;
			break;
		case 87:
//		case 38:
		case 83:
//		case 40:
			player.velY=0;
			break;
		default:
			console.log("Unknown input: "+ keyCode);
	}
}


///////////////////Set Up/////////////////////////
//////////////////////Update Window/////////////////////////
//Constantly refresh the window
function update(){
	
        //Captures player's velocity at moment of update
        var velX = player.velX;
        var velY = player.velY;
    
        //Redraw canvas, objects, obstacles and player
	paintBG();
	drawWalls(obsts);
	drawObjs(objs);
	drawPlayer();

        //Transform canvas based on player's velocity
	ctx.transform(1, 0, 0, 1, velX, velY);
	player.x -= velX;
	player.y -= velY;
      
        //Calculates collision between player and obstacles
        wallCollisions(obsts, player, velX, velY);
        
	//Set Dialog Text
	currDial=writeObjs(objs, player, currDial);

	//Re-update
	requestAnimationFrame(update);
}

//Start the update cycle
//
//
//
//
//
//

//Current default text for level

function saveGame(){
    var obj = new Object();
    obj.level = currentlevel;
    obj.stage  = currentstage;
    obj.verbal = saveJournal("verbalTable")
    obj.published = saveJournal("publishedTable")
    obj.misc = saveJournal("miscTable")
    var jsonString= JSON.stringify(obj);
    jsonString = [jsonString];
    var blob1 = new Blob(jsonString, { type: "text/plain;charset=utf-8" });
    var url = window.URL || window.webkitURL;
    link = url.createObjectURL(blob1);
    var a = document.createElement("a");
    a.download = "FakeNewsGame.save";
    a.href = link;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function loadGame(){
    inp=this
    console.log("Loading game....")
    console.log(inp.files[0])
    var fr = new FileReader();
    fr.readAsText(inp.files[0]);
    fr.addEventListener("load", e => {
        console.log(e.target.result, JSON.parse(fr.result))
        gameState=JSON.parse(fr.result)
        loadLevel(gameState.level, gameState.stage)
        loadJournal("miscTable", gameState.misc)
        loadJournal("verbalTable", gameState.verbal)
        loadJournal("publishedTable", gameState.published)
        closeNav()
    })
    document.getElementById('mapview').focus();
}


function startGame(){
///////////////////Set Up/////////////////////////
loadLevel('levels/tutorial/json/index.json', 'stage0');
//loadLevel('levels/town/json/index.json', 'stage0');
fileInput.addEventListener("change", loadGame, false);

setKeyboard();

resizeCanvas();
document.addEventListener("keyup", keyUpActions, false);
document.addEventListener("keydown", keyDownActions, false);
window.addEventListener('resize', resizeCanvas, false);
//////////////////////Update Window/////////////////////////
//Constantly refresh the window

//Start the update cycle
window.addEventListener("load", function(){
	update();
});


}
