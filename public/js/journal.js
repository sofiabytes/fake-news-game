var journalTabs="json/journalTabs.json";
var tabs={};
$.getJSON(journalTabs, function(data){
		tabs=data["tabs"];
	});

function createTable(tabs){
	var journal=document.getElementById("journal");
	var menu=document.createElement("div");
		menu.className="tab";
	//var content=document.createElement("div");
	var i;
	var tab, section, header, description, table;

	for (i in tabs){
		//console.log(i);
		//console.log(tabs[i]);
		tab=document.createElement("button");
			tab.className="tablinks";
			tab.setAttribute("onclick","openPage(event, '"+tabs[i].name+"')");
			tab.innerHTML=tabs[i].name;
		menu.appendChild(tab);
		section=document.createElement("div");
			section.id=tabs[i].name;
			section.className="tabcontent";
			section.style="display: block;";
		header=document.createElement("h3");
			header.innerHTML=tabs[i].name;
		description=document.createElement("p");
			description.innerHTML=tabs[i].text;
		table=document.createElement("table");
			table.id=tabs[i].table;
			table.border="0";
			table.width="100%";
		section.appendChild(header);
		section.appendChild(description);
		section.appendChild(table);
	journal.appendChild(section);
	
	}
	//document.getElementById("journal").appendChild(menu);
	journal.insertBefore(menu, journal.childNodes[0]);
}


//Shows one tab in the journal and hides the others
function openPage(evt, thingName) {
  // Declare all variables
  var i, tabcontent, tablinks;
  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(thingName).style.display = "block";
  evt.currentTarget.className += " active";
} 

//Save info to journal by adding table row and slider
//info - text to save / ident - identification text / journalTable - table to save to
function saveToJournal(info, sourceID, ident, journalTable) {
	var a=document.getElementById(ident);
	if (a){
		console.log(info + " is already in table " + journalTable + " with id " + ident);
            }
	else{
	console.log("Saving "+ident);
		//create table format
		var row1=document.createElement("tr");
		var td1=document.createElement("td");
		var td2=document.createElement("td");
                var td3=document.createElement("td");
		//create paragraph with info
                var paragraph=document.createElement("P");
                        paragraph.id=ident;
			paragraph.innerHTML=info;
                //Create paragraph with source origin info
                var origin=document.createElement("P");
                origin.innerHTML=sourceID
                //	$.getJSON('json/dialogue.json', function(data){
                //            origindata=data[sourceID];
                //        });
                //        origin.innerHTML="<b>Source:</b> "+origindata.title+" "+origindata.name+"<br>"
                //                        +"<b>Located:</b> "+origindata.location;
		//Create slider
		var slider=document.createElement("input");
			slider.id=ident+".slider";
			slider.type="range";
			slider.min="1";
			slider.max="3";
			slider.setAttribute("value","2");
			slider.className="slider";
			slider.setAttribute("onchange","this.setAttribute('value', this.value);");
		//Place data in page
		td1.appendChild(paragraph);
		td2.appendChild(origin);
                td3.appendChild(slider);
		row1.appendChild(td1);
		row1.appendChild(td2);
                row1.appendChild(td3);
	document.getElementById(journalTable).appendChild(row1);
	console.log("Saved "+ident);
	}
	//Open relevant journal page
	var journal;
	switch (journalTable){
	case "verbalTable":
		journal="Verbal Sources";
		break;
	case "miscTable":
		journal="Miscellaneous";
		break;
	default:
	journal="Published Sources";}
	openPage(event, journal);
}

function retrieveInfoStatus(id){
    const status = new Map([
        ["1", "False"],
        ["2", 0],
        [null, 0],
        ["3", "True"]
]);
    console.log(id+".slider")
    Inforow=document.getElementById(id+".slider")
    console.log('SLider value: '+Inforow['value'])
    if (Inforow){
        console.log('returning vlaue')
        return status.get(Inforow['value']);
    }
    return 0
}


createTable(tabs);

openPage(event, 'Published Sources');


function saveJournal(tableID){
    var table=document.getElementById(tableID)
    console.log(table)
    var data = [];
    var headers = ["name", "source", "rating"];
    for (var i=0; i<table.rows.length; i++) {
        console.log('i')
        console.log(i)
        var tableRow = table.rows[i];
        var rowData = {};
        for (var j=0; j<tableRow.cells.length; j++) {
        console.log('j')
        console.log(j)
        rowData[ headers[j] ] = tableRow.cells[j].innerHTML;
        }
        data.push(rowData);
    }
    return data;
}

function loadJournal(tableID, tableData){
    console.log("Load Journal Placeholder")
    console.log(tableID)
    var table=document.getElementById(tableID)
    console.log(table)
    console.log(tableData)
    for (let r = 0; r < tableData.length; r++) {
        console.log("Looping throug")
        console.log(tableData[r])
        var html ="<table><tbody><tr>"+"<td>"+tableData[r].name+"</td><td>"+tableData[r].source+"</td><td>"+tableData[r].rating+"</td></tr></tbody></table>"
        console.log("creating ROW")
        console.log(html)
        var range = document.createRange();
        range.selectNodeContents(table);
        var frag = range.createContextualFragment(html);
        table.appendChild(frag);
    }
}

